﻿namespace SmartCityRecognitionApi.ViewModels
{
    public class ObservationView
    {
        public string PollutantId { get; set; }
        public string Value { get; set; }
        public string Datetime { get; set; }
        public int Order { get; set; }
    }
}
