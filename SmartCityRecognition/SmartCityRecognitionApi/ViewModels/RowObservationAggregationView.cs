﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartCityRecognitionApi.ViewModels
{
    public class RowObservationAggregationView
    {
        public RowObservationAggregationView()
        {
            Aggregations = new List<AggregationView>();
        }

        public StationView Station { get; set; }

        public List<AggregationView> Aggregations { get; set; }
    }
}
