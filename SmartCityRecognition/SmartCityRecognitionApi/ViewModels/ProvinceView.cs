﻿namespace SmartCityRecognitionApi.ViewModels
{
    public class ProvinceView
    {
        public int Id { get;  set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public decimal MaxTemp { get; internal set; }
        public decimal MinTemp { get; internal set; }
        public decimal Rain { get; internal set; }
    }
}
