﻿using System.Collections.Generic;

namespace SmartCityRecognitionApi.ViewModels
{
    public class RowObservationView
    {
        public RowObservationView()
        {
            Observations = new List<ObservationView>();
        }

        public StationView Station { get; set; }

        public List<ObservationView> Observations { get; set; }
    }
}
