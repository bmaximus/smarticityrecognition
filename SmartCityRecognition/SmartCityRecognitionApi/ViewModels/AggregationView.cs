﻿namespace SmartCityRecognitionApi.ViewModels
{
    public class AggregationView
    {

        public int Order { get; set; }

        public string Value { get; internal set; }
    }
}
