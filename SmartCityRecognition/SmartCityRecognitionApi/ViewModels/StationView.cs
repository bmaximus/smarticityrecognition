﻿namespace SmartCityRecognitionApi.ViewModels
{
    public class StationView
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public int Order { get; set; }
    }
}
