﻿namespace SmartCityRecognitionApi.ViewModels
{
    public class PollutantView
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public int Order { get; set; }
    }
}
