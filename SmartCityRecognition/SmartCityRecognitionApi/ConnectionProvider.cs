﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace SmartCityRecognitionApi
{
    public class ConnectionProvider : IDisposable
    {
        private SqlConnection _Connection;

        private string ConnectionString => "Server=tcp:smarticityrecognition.database.windows.net,1433;Initial Catalog=SmartCityRecognition;Persist Security Info=False;User ID=dt;Password=dreamteam3#;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=5000;";

        public ConnectionProvider()
        {
            _Connection = new SqlConnection(ConnectionString);
            _Connection.Open();
        }

        public void ExecuteNonQuery(string cmd)
        {
            using (var command = new SqlCommand(cmd, _Connection))
            {
                command.ExecuteNonQuery();
            }
        }

        public string ExecuteDataReader(string cmd)
        {
            var da = new SqlDataAdapter(cmd, _Connection);
            var dt = new DataTable();
            try
            {
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                 
            }

            if (dt.Rows.Count > 0)
            {
                return (dt.Rows[0][0] == null ? "" : dt.Rows[0][0].ToString());
            }
            return "";
        }

        public List<T> ExecuteDataReader<T>(string cmd)
        {
            var da = new SqlDataAdapter(cmd, _Connection);
            var dt = new DataTable();
            try
            {
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                 
            }
            var data = new List<T>();

            var properties = typeof(T).GetProperties();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var newTtypeObj = Activator.CreateInstance<T>();
                foreach (var property in properties)
                {
                    property.SetValue(newTtypeObj, dt.Rows[i][property.Name].ToString());
                }

                data.Add(newTtypeObj);
            }
            return data;
        }

        public void Dispose()
        {
            _Connection.Close();
            _Connection.Dispose();
        }

        ~ConnectionProvider()
        {
            if (_Connection.State != ConnectionState.Closed)
            {
                _Connection.Close();
                _Connection.Dispose();
            }
            _Connection = null;
        }
    }
}