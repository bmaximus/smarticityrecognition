﻿using Microsoft.AspNetCore.Mvc;
using SmartCityRecognitionApi.Models;
using SmartCityRecognitionApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCityRecognitionApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SmartController : ControllerBase
    {
        [HttpGet("getpollutants")]
        public List<PollutantView> GetPollutants()
        {
            var data = new List<PollutantView>();
            using (var connectionProvider = new ConnectionProvider())
            {
                var dbService = new DatabaseServices(connectionProvider);
                var pollutans = dbService.GetPollutants(true);

                data = pollutans.GroupBy(d => new { d.Order, Name = $"{d.Name} - {d.Uom}" })
                                .Select(s => new PollutantView() { Id = s.Min(a => a.Id), Name = s.Key.Name, Order = Convert.ToInt32(s.Key.Order) })
                                .ToList();
            }
            return data;
        }

        [HttpGet("getaggregationpollutants")]
        public List<PollutantView> GetAggregationPollutants()
        {
            var data = new List<PollutantView>();

            using (var connectionProvider = new ConnectionProvider())
            {
                var databaseService = new DatabaseServices(connectionProvider);
                data = databaseService.GetAggregationPollutants()
                                      .GroupBy(d => new { d.Order, Name = $"{d.Name} - {d.Uom}" })
                                      .Select(s => new PollutantView()
                                      {
                                          Id = s.Min(a => a.Id),
                                          Name = s.Key.Name,
                                          Order = Convert.ToInt32(s.Key.Order)
                                      })
                                      .ToList();

            }
            return data;
        }

        [HttpGet("getprovinces")]
        public List<ProvinceView> GetProvinces()
        {
            var data = new List<ProvinceView>();
            using (var connectionProvider = new ConnectionProvider())
            {
                var databaseService = new DatabaseServices(connectionProvider);

                var observations = databaseService.GetAggregationObservations()
                                                  .GroupBy(a => new { a.StationId, a.Order })
                                                  .OrderBy(d => d.Key.Order)
                                                  .ToList();

                var provinces = databaseService.GetProvinces();

                foreach (var province in provinces)
                {
                    var pv = new ProvinceView
                    {
                        Id = Convert.ToInt32(province.Id),
                        Name = province.Name,


                        MaxTemp = observations.Where(d => province.StationIds.Contains(d.Key.StationId) && d.Any(s => s.PollutantId == "2")).Average(x => x.Average(z => Utils.Parse(z.Value))),

                        MinTemp = observations.Where(d => province.StationIds.Contains(d.Key.StationId) && d.Any(s => s.PollutantId == "3")).Average(x => x.Average(z => Utils.Parse(z.Value))),

                        Rain = observations.Where(d => province.StationIds.Contains(d.Key.StationId) && d.Any(s => s.PollutantId == "11")).Average(x => x.Average(z => Utils.Parse(z.Value)))

                    };

                    data.Add(pv);
                }

                var maxTemp = data.GroupBy(s => s.MaxTemp).OrderByDescending(d=>d.Key).FirstOrDefault();
                var minTemp = data.GroupBy(s => s.MinTemp).OrderBy(d=>d.Key).FirstOrDefault();
                var maxRain = data.GroupBy(s => s.Rain).OrderByDescending(d=>d.Key).FirstOrDefault();

                foreach (var item in data)
                {
                    var sb = new StringBuilder();
                    if(maxTemp != null && item.MaxTemp == maxTemp.Key)
                    {
                        sb.AppendLine(Utils.GetRandomMax());
                    }

                    if (minTemp != null && item.MinTemp == minTemp.Key)
                    {
                        sb.AppendLine(Utils.GetRandomMin());
                    }

                    if (maxRain != null && item.Rain == maxRain.Key && maxRain.Key > 0)
                    {
                        sb.AppendLine(Utils.GetRandomRain());
                    }
                    item.Text += sb.ToString();

                    if(item.Text.Length == 0)
                    {
                        item.Text = "Boring.. dont go there.. ";
                    }
                }
            }
            return data;
        }

        [HttpGet("getobservations")]
        public List<RowObservationView> GetObservations()
        {
            var data = new List<RowObservationView>();
            using (var connectionProvider = new ConnectionProvider())
            {
                var dbService = new DatabaseServices(connectionProvider);
                var observations = dbService.GetObservations();
                var maxDate = observations.GroupBy(d => Utils.ConvertToDateTime(d.Datetime)).Max(s => s.Key);

                //var stations = dbService.GetStations().Select(s => new StationView() { Id = s.Id, Name = s.Name }).ToList();
                var provinces = dbService.GetProvinces();
                var pollutants = dbService.GetPollutants(true)
                                          .GroupBy(d => new { d.Order, Name = $"{d.Name} - {d.Uom}" })
                                          .Select(a => new { Name = a.Key, a.Key.Order, Ids = a.Select(d => d.Id).OrderBy(idOrder => idOrder).ToList() });

                foreach (var province in provinces)
                {
                    var row = new RowObservationView
                    {
                        Station = new StationView() { Id = province.Id, Name = province.Name }
                    };
                    foreach (var pollutant in pollutants)
                    {
                        var matchedValue = observations
                                                .Where(a => province.StationIds.Contains(a.StationId) &&
                                                            pollutant.Ids.Contains(a.PollutantId) &&
                                                            Utils.ConvertToDateTime(a.Datetime) == maxDate)
                                                .GroupBy(s => s.Datetime)
                                                .Select(a => new ObservationView
                                                {
                                                    Datetime = a.Key,
                                                    Value = a.Min(d => d.Value),
                                                    PollutantId = pollutant.Ids.Min(),
                                                    Order = Convert.ToInt32(pollutant.Order)
                                                }).FirstOrDefault();

                        if (matchedValue != null)
                        {
                            row.Observations.Add(matchedValue);
                        }
                    }
                    data.Add(row);
                }
                data = data.Where(d => d.Station != null && d.Observations.Count == pollutants.Count()).ToList();
            }
            return data;
        }

        [HttpGet("getaggregationdata")]
        public List<RowObservationAggregationView> GetAggregationData(string provinceId)
        {
            var data = new List<RowObservationAggregationView>();

            using (var connectionProvider = new ConnectionProvider())
            {
                var databaseService = new DatabaseServices(connectionProvider);
                var observations = databaseService.GetAggregationObservations().GroupBy(a => new { a.StationId, a.Order }).OrderBy(d => d.Key.Order).ToList();

                var stations = databaseService.GetStations(provinceId).Select(s => new StationView() { Id = s.Id, Name = s.Name }).ToList();

                var pollutants = databaseService.GetAggregationPollutants()
                                          .GroupBy(d => new { d.Order, Name = $"{d.Name} - {d.Uom}" })
                                          .Select(a => new { Name = a.Key, a.Key.Order, Ids = a.Select(d => d.Id).OrderBy(idOrder => idOrder).ToList() }).ToList();

                foreach (var station in stations)
                {
                    var row = new RowObservationAggregationView
                    {
                        Station = station
                    };

                    foreach (var pollutant in pollutants)
                    {
                        foreach (var aggregations in observations.Where(a => a.Key.StationId == station.Id &&
                                                                    a.Key.Order == pollutant.Order)

                                                          .ToList())
                        {
                            row.Aggregations.Add(new AggregationView()
                            {
                                Value = aggregations.Average(d => Utils.Parse(d.Value)).ToString("#0.00"),
                                Order = Convert.ToInt32(aggregations.Key.Order),
                            });
                        }
                        row.Aggregations = row.Aggregations.OrderBy(d => d.Order).ToList();
                    }
                    data.Add(row);
                }
                data = data.Where(d => d.Station != null && d.Aggregations.Count == pollutants.Count()).ToList();

            }
            return data;
        }

        [HttpGet("getgraphdata")]
        public GraphData GetGraphData(string provinceId, string pollutantId)
        {
            var data = new GraphData();

            using (var connectionProvider = new ConnectionProvider())
            {
                var databaseService = new DatabaseServices(connectionProvider);

                var stations = databaseService.GetStations(provinceId);
                var pollutant = databaseService.GetAggregationPollutants().FirstOrDefault(d => d.Id == pollutantId);

                if (pollutant == null) { return data; }

                var observations = databaseService.GetAggregationObservations().GroupBy(a => new { a.StationId, a.Order }).OrderBy(d => d.Key.Order).ToList();

                var aggObservations = observations.Where(d => stations.Any(s => s.Id == d.Key.StationId) &&
                                                             d.Key.Order == pollutant.Order)
                                                  .GroupBy(a => new
                                                  {
                                                      a.Key.StationId,
                                                      Value = a.Average(d => Utils.Parse(d.Value)).ToString("#0.00")
                                                  }).ToList();

                for (int i = 0; i < aggObservations.Count; i++)
                {
                    var fodStation = stations.FirstOrDefault(s => s.Id == aggObservations[i].Key.StationId);
                    if (fodStation != null)
                    {
                        data.Stations.Add(new StationView() { Name = fodStation.Name, Order = i });
                        data.AggregationViews.Add(new AggregationView() { Value = aggObservations[i].Key.Value, Order = i });
                    }
                }
            }
            return data;
        }
    }
}
