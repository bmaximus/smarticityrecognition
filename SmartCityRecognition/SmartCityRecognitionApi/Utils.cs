﻿using System;
using System.Collections.Generic;

namespace SmartCityRecognitionApi
{
    public static class Utils
    {
        public static DateTime ConvertToDateTime(string datetime)
        {
            if (DateTime.TryParse(datetime, out var dt))
            {

            }
            return dt;
        }

        public static decimal Parse(string val)
        {
            return decimal.Parse(val);
        }

        private static List<string> HotTexts = new List<string>()
        {
                "In a relationship with Air Conditioner",
                "It's hot outside"
        };

        private static List<string> ColdTexts = new List<string>()
        {
            "Hot chocolate weather",
            "It's I can't feel my face degrees",
            "Happiness is a hot chocolate drink on a cold day",
            "It's cold outside"
        };

        private static List<string> RainTexts = new List<string>()
        {
            "Rain, rain go away",
            "You propably will need an umbrella"
        };

        private static Random rand = new Random(DateTime.Now.Millisecond);

        public static string GetRandomMax()
        {
            return HotTexts[rand.Next(0, HotTexts.Count - 1)];
        }
        public static string GetRandomMin()
        {
            return ColdTexts[rand.Next(0, ColdTexts.Count - 1)];
        }
        public static string GetRandomRain()
        {
            return RainTexts[rand.Next(0, RainTexts.Count - 1)];
        }

    }
}
