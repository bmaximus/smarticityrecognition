﻿using SmartCityRecognitionApi.Models;
using System;
using System.IO;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace SmartCityRecognitionApi
{
    public class ScheduledServices
    {
        public void AirQualityGatherer()
        {
            string uri = "http://www.airquality.dli.mlsi.gov.cy/air/airquality.php";
            System.Net.HttpWebRequest req = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(uri);
            req.Timeout = 1000 * 60 * 10; // milliseconds
            System.Net.WebResponse res = req.GetResponse();
            air_quality result = null;
            using (Stream responseStream = res.GetResponseStream())
            {
                XmlSerializer serializer = new XmlSerializer(typeof(air_quality));
                result = (air_quality)serializer.Deserialize(responseStream);
            }

            if (result != null && result.pollutants.Length > 0)
            {
                using (var connection = new ConnectionProvider())
                {
                    try
                    {
                        var dServices = new DatabaseServices(connection);
                        dServices.CreateMissingStations(result);
                        dServices.CeateMissingPollutants(result);

                        var stations = dServices.GetStations();
                        var pollutants = dServices.GetPollutants();

                        dServices.AddObservations(result, stations, pollutants);
                    }
                    catch (Exception ex)
                    {

                         
                    }
                }
            }
        }

        public void MetereologicalGatherer()
        {
            string uri = "http://weather.cyi.ac.cy/data/met/CyDoM.xml";
            XDocument document = XDocument.Load(uri);
            meteorology result = null;

            XmlSerializer serializer = new XmlSerializer(typeof(meteorology));
            using (var reader = document.CreateReader())
            {
                result = (meteorology)serializer.Deserialize(reader);
            }

            if (result != null && result.observations.Length > 0)
            {
                using (var connection = new ConnectionProvider())
                {
                    try
                    {
                        var dServices = new DatabaseServices(connection);
                        dServices.CreateMissingStations(result);
                        dServices.CeateMissingPollutants(result);

                        var stations = dServices.GetStations();
                        var pollutants = dServices.GetPollutants();

                        dServices.AddObservations(result, stations, pollutants);
                    }
                    catch (Exception ex)
                    {

                         
                    }
                }
            }
        }
    }
}
