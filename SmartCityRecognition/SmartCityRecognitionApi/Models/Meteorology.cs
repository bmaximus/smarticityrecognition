﻿namespace SmartCityRecognitionApi.Models
{
    [System.Serializable()]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true)]
    [System.Xml.Serialization.XmlRoot(Namespace = "", IsNullable = false)]
    public partial class meteorology
    {
        private meteorologyStation[] stationsField;

        /// <remarks/>
        public string copyright { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItem("station", IsNullable = false)]
        public meteorologyStation[] stations
        {
            get => this.stationsField;
            set => this.stationsField = value;
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElement("observations")]
        public meteorologyObservations[] observations { get; set; }
    }

    /// <remarks/>
    [System.Serializable()]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true)]
    public partial class meteorologyStation
    {
        private decimal station_latitudeField;

        /// <remarks/>
        public string station_code { get; set; }

        /// <remarks/>
        public decimal station_latitude
        {
            get => this.station_latitudeField;
            set => this.station_latitudeField = value;
        }

        /// <remarks/>
        public decimal station_longitude { get; set; }
    }

    /// <remarks/>
    [System.Serializable()]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true)]
    public partial class meteorologyObservations
    {
        private string date_timeField;

        /// <remarks/>
        public string station_code { get; set; }

        /// <remarks/>
        public string date_time
        {
            get => this.date_timeField;
            set => this.date_timeField = value;
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElement("observation")]
        public meteorologyObservationsObservation[] observation { get; set; }
    }

    /// <remarks/>
    [System.Serializable()]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true)]
    public partial class meteorologyObservationsObservation
    {
        private string observation_valueField;

        /// <remarks/>
        public string observation_name { get; set; }

        /// <remarks/>
        public string observation_value
        {
            get => this.observation_valueField;
            set => this.observation_valueField = value;
        }

        /// <remarks/>
        public string observation_unit { get; set; }
    }


}