﻿namespace SmartCityRecognitionApi.Models
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.Serializable()]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true)]
    [System.Xml.Serialization.XmlRoot(Namespace = "", IsNullable = false)]
    public partial class air_quality
    {
        private air_qualityStation[] stationsField;

        /// <remarks/>
        public air_qualityCopyright copyright { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItem("station", IsNullable = false)]
        public air_qualityStation[] stations
        {
            get => this.stationsField;
            set => this.stationsField = value;
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItem("pollutant", IsNullable = false)]
        public air_qualityPollutant[] pollutants { get; set; }
    }

    /// <remarks/>
    [System.Serializable()]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true)]
    public partial class air_qualityCopyright
    {

        /// <remarks/>
        public string copyright { get; set; }
    }

    /// <remarks/>
    [System.Serializable()]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true)]
    public partial class air_qualityStation
    {
        private byte pollutant_codeField;
        private string date_timeField;
        private string station_name_grField;
        private string station_type_grField;
        private decimal station_longitudeField;

        /// <remarks/>
        public byte station_code { get; set; }

        /// <remarks/>
        public byte pollutant_code
        {
            get => this.pollutant_codeField;
            set => this.pollutant_codeField = value;
        }

        /// <remarks/>
        public decimal pollutant_value { get; set; }

        /// <remarks/>
        public string date_time
        {
            get => this.date_timeField;
            set => this.date_timeField = value;
        }

        /// <remarks/>
        public string station_name_en { get; set; }

        /// <remarks/>
        public string station_name_gr
        {
            get => this.station_name_grField;
            set => this.station_name_grField = value;
        }

        /// <remarks/>
        public string station_type_en { get; set; }

        /// <remarks/>
        public string station_type_gr
        {
            get => this.station_type_grField;
            set => this.station_type_grField = value;
        }

        /// <remarks/>
        public decimal station_latitude { get; set; }

        /// <remarks/>
        public decimal station_longitude
        {
            get => this.station_longitudeField;
            set => this.station_longitudeField = value;
        }
    }

    /// <remarks/>
    [System.Serializable()]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true)]
    public partial class air_qualityPollutant
    {
        private string codeField;
        private string name_enField;

        /// <remarks/>
        public byte id { get; set; }

        /// <remarks/>
        public string code
        {
            get => this.codeField;
            set => this.codeField = value;
        }

        /// <remarks/>
        public string uom_en { get; set; }

        /// <remarks/>
        public string name_en
        {
            get => this.name_enField;
            set => this.name_enField = value;
        }

        /// <remarks/>
        public string name_gr { get; set; }
    }


}
