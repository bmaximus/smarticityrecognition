﻿using SmartCityRecognitionApi.ViewModels;
using System.Collections.Generic;

namespace SmartCityRecognitionApi.Models
{
    public class GraphData
    {
        public GraphData()
        {
            Stations = new List<StationView>();
            AggregationViews = new List<AggregationView>();
        }

        public List<StationView> Stations { get; set; }

        public List<AggregationView> AggregationViews { get; set; }
    }
}
