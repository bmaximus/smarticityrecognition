﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace SmartCityRecognitionApi
{
    public class Program
    {
        public static void Main(string[] args)
        {

            var ss = new ScheduledServices();

            Task.Run(() =>
            {
                while (true)
                {
                    try
                    {
                        ss.MetereologicalGatherer();
                    }
                    catch (Exception)
                    {
                    }
                    Thread.Sleep(new TimeSpan(0, 30, 0));

                }
            });

            Task.Run(() =>
            {
                while (true)
                {
                    try
                    {
                        ss.AirQualityGatherer();
                    }
                    catch (Exception)
                    {

                    }
                    Thread.Sleep(new TimeSpan(0, 31, 0));
                }
            });


            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
