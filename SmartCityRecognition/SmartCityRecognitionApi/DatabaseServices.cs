
using SmartCityRecognitionApi.Entities;
using SmartCityRecognitionApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCityRecognitionApi
{
    public class DatabaseServices
    {
        private ConnectionProvider connectionProvider;

        public DatabaseServices(ConnectionProvider connectionProvider)
        {
            this.connectionProvider = connectionProvider;
        }

        public void AddObservations(meteorology result, List<Station> stations, List<Pollutant> pollutants)
        {
            try
            {
                for (int i = 0; i < result.observations.Length; i++)
                {
                    var aggregations = new List<Aggregation>();
                    var update = false;
                    var queryObservaton = new StringBuilder();
                    queryObservaton.AppendLine("INSERT INTO[dbo].[Observation] ");
                    queryObservaton.AppendLine(" ([PollutantId], [StationId], [Value], [DateTime]) VALUES ");

                    var station = stations.FirstOrDefault(a => string.Compare(a.Code, result.observations[i].station_code.ToString(), true) == 0);

                    for (int j = 0; j < result.observations[i].observation.Length; j++)
                    {
                        var pollutant = pollutants.FirstOrDefault(a => a.Code == result.observations[i].observation[j].observation_name);
                        if (station != null && pollutant != null)
                        {
                            var sb = new StringBuilder();
                            sb.AppendLine("select max(DateTime) ");
                            sb.AppendLine("from OBSERVATION  ");
                            sb.AppendLine($"where Pollutantid = {pollutant.Id} AND ");
                            sb.AppendLine($" StationId = {station.Id}");
                            var latestDbTime = connectionProvider.ExecuteDataReader(sb.ToString());

                            if (string.IsNullOrWhiteSpace(latestDbTime) ||
                                !string.IsNullOrWhiteSpace(latestDbTime) &&
                                DateTime.TryParse(latestDbTime, out var latestDateTime) &&
                                DateTime.TryParse(result.observations[i].date_time, out var observationDateTime) &&
                                observationDateTime > latestDateTime)
                            {
                                update = true;
                            }

                            var observation = new
                            {
                                PollutanId = pollutant.Id,
                                StationId = station.Id,
                                DateTime = result.observations[i].date_time.ToString()
                            };

                            var val = result.observations[i].observation[j].observation_value.ToString();
                            var value = val;

                            if (val.Contains("("))
                            {
                                value = val.Split('(')[0].Trim();
                                var agg = new Aggregation()
                                {
                                    PollutantId = Convert.ToInt32(observation.PollutanId),
                                    StationId = Convert.ToInt32(observation.StationId),
                                    OriginalMessage = val,
                                    Value = CalculateValueFromAggregation(val)
                                };
                                aggregations.Add(agg);
                            }
                                                        
                            queryObservaton.AppendLine($"({observation.PollutanId}, {observation.StationId}, " +
                                                       $"'{value}', '{observation.DateTime}' )");
                        }

                        if (j != result.observations[i].observation.Length - 1)
                        {
                            queryObservaton.Append(", ");
                        }
                    }

                    if (result.observations[i].observation.Length > 0 && update)
                    {
                        connectionProvider.ExecuteNonQuery(queryObservaton.ToString());
                    }

                    AggregationInsert(aggregations, update);
                }
            }
            catch (Exception)
            {

            }
        }

        public void AddObservations(air_quality result, List<Station> stations, List<Pollutant> pollutants)
        {
            try
            {
                var aggregations = new List<Aggregation>();

                var update = false;
                var queryObservaton = new StringBuilder();
                queryObservaton.AppendLine("INSERT INTO[dbo].[Observation] ");
                queryObservaton.AppendLine(" ([PollutantId], [StationId], [Value], [DateTime]) VALUES ");

                for (int i = 0; i < result.stations.Length; i++)
                {
                    var station = stations.FirstOrDefault(a => string.Compare(a.Code, result.stations[i].station_code.ToString(), true) == 0);

                    var pollutant = pollutants.Where(a => int.TryParse(a.Code, out var cc)).FirstOrDefault(a => Convert.ToInt32(a.Code) == Convert.ToInt32(result.stations[i].pollutant_code));

                    if (station != null && pollutant != null)
                    {
                        var sb = new StringBuilder();
                        sb.AppendLine("select max(DateTime) ");
                        sb.AppendLine("from OBSERVATION  ");
                        sb.AppendLine($"where Pollutantid = {pollutant.Id} AND ");
                        sb.AppendLine($" StationId = {station.Id}");
                        var latestDbTime = connectionProvider.ExecuteDataReader(sb.ToString());

                        if (string.IsNullOrWhiteSpace(latestDbTime) ||
                           (!string.IsNullOrWhiteSpace(latestDbTime) &&
                            DateTime.TryParse(latestDbTime, out var latestDateTime) &&
                            DateTime.TryParse(result.stations[i].date_time, out var observationDateTime) &&
                            observationDateTime > latestDateTime))
                        {
                            update = true;
                        }


                        var observation = new
                        {
                            PollutanId = pollutant.Id,
                            StationId = station.Id,
                            DateTime = result.stations[i].date_time.ToString()
                        };

                        var val = result.stations[i].pollutant_value.ToString();
                        var value = val;

                        if (val.Contains("("))
                        {
                            value = val.Split('(')[0].Trim();
                            var agg = new Aggregation()
                            {
                                PollutantId = Convert.ToInt32(observation.PollutanId),
                                StationId = Convert.ToInt32(observation.StationId),
                                OriginalMessage = val,
                                Value = CalculateValueFromAggregation(val)
                            };
                            aggregations.Add(agg);
                        }

                        queryObservaton.AppendLine($"({observation.PollutanId}, {observation.StationId}, " +
                                                   $"'{value}', '{observation.DateTime}' )");
                    }

                    if (i != result.stations.Length - 1)
                    {
                        queryObservaton.Append(", ");
                    }
                }

                if (result.stations.Length > 0 && update)
                {
                    connectionProvider.ExecuteNonQuery(queryObservaton.ToString());
                }

                AggregationInsert(aggregations, update);

            }
            catch (Exception)
            {


            }

        }

        private void AggregationInsert(List<Aggregation> aggregations, bool update)
        {
            if (aggregations.Count > 0 && update)
            {
                var aggText = new StringBuilder();
                aggText.AppendLine(" INSERT INTO [dbo].[ObservationAggregation] ");
                aggText.AppendLine(" ([StationId], [PollutantId], [OriginalMessage], [Value]) ");
                aggText.AppendLine(" VALUES ");

                for (int i = 0; i < aggregations.Count; i++)
                {
                    aggText.AppendLine($" ({aggregations[i].StationId}, {aggregations[i].PollutantId}, '{aggregations[i].OriginalMessage}', '{aggregations[i].Value}') ");

                    if (i != aggregations.Count - 1)
                    {
                        aggText.Append(", ");
                    }
                }
                connectionProvider.ExecuteNonQuery(aggText.ToString());
            }
        }

        private string CalculateValueFromAggregation(string val)
        {
            var splitted = val.Split('=');
            if(splitted.Length == 2)
            {
                var cleanSplit = splitted[1].Split(',');
                val = cleanSplit[0].Trim();
            }
            return val;
        }

        public void CeateMissingPollutants(meteorology result)
        {
            try
            {
                var pollutants = GetPollutants();

                var missingPollutants = result.observations
                                              .SelectMany(s => s.observation.Select(w => new { w.observation_unit, w.observation_name }))
                                               .Where(a => !pollutants.Any(s => string.Compare(s.Code, a.observation_name.ToString(), true) == 0))
                                               .GroupBy(a => new { a.observation_name, a.observation_unit })
                                               .Distinct()
                                               .ToList();

                var addPollutant = new StringBuilder();

                addPollutant.AppendLine("INSERT INTO [dbo].[Pollutant]");
                addPollutant.AppendLine(" ([Code], [Name], [Uom]) ");
                addPollutant.AppendLine(" VALUES ");
                if (missingPollutants.Count > 0)
                {
                    for (int i = 0; i < missingPollutants.Count; i++)
                    {
                        addPollutant.AppendLine($"( '{missingPollutants[i].Key.observation_name}',  '{missingPollutants[i].Key.observation_name}', '{missingPollutants[i].Key.observation_unit}'  )");
                        if (i != missingPollutants.Count - 1)
                        {
                            addPollutant.Append(", ");
                        }
                    }
                    connectionProvider.ExecuteNonQuery(addPollutant.ToString());
                }

            }
            catch (Exception)
            {


            }
        }

        public void CeateMissingPollutants(air_quality result)
        {
            try
            {
                var pollutants = GetPollutants();

                var missingPollutants = result.pollutants.Where(a => !pollutants.Any(s => string.Compare(s.Code, a.id.ToString(), true) == 0)).GroupBy(a => new { a.id, a.name_en, a.uom_en }).Distinct().ToList();

                var addPollutant = new StringBuilder();

                addPollutant.AppendLine("INSERT INTO [dbo].[Pollutant]");
                addPollutant.AppendLine(" ([Code], [Name], [Uom]) ");
                addPollutant.AppendLine(" VALUES ");
                if (missingPollutants.Count > 0)
                {
                    for (int i = 0; i < missingPollutants.Count; i++)
                    {
                        addPollutant.AppendLine($"( '{missingPollutants[i].Key.id}',  '{missingPollutants[i].Key.name_en}', '{missingPollutants[i].Key.uom_en}'  )");
                        if (i != missingPollutants.Count - 1)
                        {
                            addPollutant.Append(", ");
                        }
                    }
                    connectionProvider.ExecuteNonQuery(addPollutant.ToString());
                }
            }
            catch (Exception)
            {
            }

        }

        public void CreateMissingStations(air_quality result)
        {
            try
            {
                var stations = GetStations();

                var missingStations = result.stations.Where(a => !stations.Any(s => string.Compare(s.Code, a.station_code.ToString(), true) == 0)).GroupBy(a => new { a.station_code, a.station_name_en }).Distinct().ToList();

                var addStations = new StringBuilder();

                addStations.AppendLine("INSERT INTO [dbo].[Station]");
                addStations.AppendLine(" ([Code], [Name]) ");
                addStations.AppendLine(" VALUES ");
                if (missingStations.Count > 0)
                {
                    for (int i = 0; i < missingStations.Count; i++)
                    {
                        addStations.AppendLine($"( '{missingStations[i].Key.station_code}',  '{missingStations[i].Key.station_name_en}' )");
                        if (i != missingStations.Count - 1)
                        {
                            addStations.Append(", ");
                        }
                    }
                    connectionProvider.ExecuteNonQuery(addStations.ToString());
                }

            }
            catch (Exception)
            {

            }
        }

        public void CreateMissingStations(meteorology result)
        {
            try
            {
                var stations = GetStations();

                var missingStations = result.stations.Where(a =>
                                    !stations.Any(s =>
                                    string.Compare(s.Code, a.station_code.ToString(), true) == 0))

                                    .GroupBy(a => new { a.station_code }).Distinct().ToList();

                var addStations = new StringBuilder();

                addStations.AppendLine("INSERT INTO [dbo].[Station]");
                addStations.AppendLine(" ([Code], [Name]) ");
                addStations.AppendLine(" VALUES ");
                if (missingStations.Count > 0)
                {
                    for (int i = 0; i < missingStations.Count; i++)
                    {
                        addStations.AppendLine($"( '{missingStations[i].Key.station_code}',  '{missingStations[i].Key.station_code}' )");
                        if (i != missingStations.Count - 1)
                        {
                            addStations.Append(", ");
                        }
                    }
                    connectionProvider.ExecuteNonQuery(addStations.ToString());
                }

            }
            catch (Exception)
            {


            }
        }

        public List<Station> GetStations(string provinceId = "")
        {
            var q = new StringBuilder();
            q.AppendLine("SELECT Id, Code, Name");
            q.AppendLine("FROM STATION");

            if (!string.IsNullOrWhiteSpace(provinceId) && Int32.TryParse(provinceId, out var provinceid))
            {
                q.AppendLine(" s inner join ProvinceStation ps on s.Id = ps.StationId and ps.ProvinceId = "+ provinceid.ToString());
            }

            var data = connectionProvider.ExecuteDataReader<Station>(q.ToString());
            return data;
        }

        public List<Province> GetProvinces()
        {
            var q = new StringBuilder();
            q.AppendLine("SELECT p.id, p.[Name], s.Id AS StationId ");
            q.AppendLine(" FROM Province p ");
            q.AppendLine(" inner join ProvinceStation ps ");
            q.AppendLine(" on p.Id = ps.ProvinceId ");
            q.AppendLine(" inner join Station s ");
            q.AppendLine(" on ps.StationId = s.Id ");
            q.AppendLine(" and p.Disabled = 0 ");

            var data = connectionProvider.ExecuteDataReader<ProvinceMap>(q.ToString())
                                         .GroupBy(s => new { s.Id, s.Name })
                                         .Select(s => new Province()
                                         {
                                            Id = s.Key.Id,
                                            Name = s.Key.Name,
                                            StationIds = s.Select(d => d.StationId).ToList()
                                         })
                                         .ToList();
            return data;
        }

        public List<Pollutant> GetPollutants(bool onlyEnabled = false)
        {
            var q = new StringBuilder();
            q.AppendLine("SELECT * ");
            q.AppendLine("FROM POLLUTANT");
            if (onlyEnabled)
            {
                q.AppendLine(" where Disable = 0 ");
                q.AppendLine(" order by [Order] ");
            }

            var data = connectionProvider.ExecuteDataReader<Pollutant>(q.ToString());
            return data;
        }

        public List<Pollutant> GetAggregationPollutants()
        {
            var q = @"select distinct p.* from ObservationAggregation oa   inner join Pollutant p  on oa.PollutantId = p.Id  and p.[Disable] = 0  order by p.[Order]";

            var data = connectionProvider.ExecuteDataReader<Pollutant>(q);
            return data;
        }

        public List<Observation> GetObservations()
        {
            var q = new StringBuilder();
            q.AppendLine("SELECT *");
            q.AppendLine("FROM OBSERVATION O INNER JOIN POLLUTANT P ON O.PollutantId = P.Id");
            q.AppendLine("WHERE  p.Disable = 0");
            var data = connectionProvider.ExecuteDataReader<Observation>(q.ToString());
            return data;
        }

        public List<AggregationObservation> GetAggregationObservations()
        {
            var q = new StringBuilder();
            q.AppendLine(" SELECT s.Id as StationId, oa.[Value], p.Id as PollutantId, p.[Order] ");
            //  q.AppendLine(" SELECT s.Id as StationId, FORMAT(AVG(CONVERT(DECIMAL(18,2), oa.[Value])), '#0.00') as [Value], p.Id as PollutantId, p.[Order] ");
            q.AppendLine(" from ObservationAggregation oa  ");
            q.AppendLine(" inner join Station s  ");
            q.AppendLine(" 	on oa.StationId = s.Id ");
            q.AppendLine(" inner join Pollutant p  ");
            q.AppendLine(" on oa.PollutantId = p.Id ");
            q.AppendLine(" and p.[Disable] = 0 ");
           // q.AppendLine(" group by s.Id, p.[Order], p.Id ");
            q.AppendLine(" order by StationId, p.[Order] ");

            var data = connectionProvider.ExecuteDataReader<AggregationObservation>(q.ToString());
            return data;
        }
    }
}