﻿namespace SmartCityRecognitionApi.Entities
{
    public class AggregationObservation
    {
        public string StationId { get; set; }

        public string PollutantId { get; set; }

        public string Value { get; set; }

        public string Order { get; set; }
    }
}
