﻿namespace SmartCityRecognitionApi.Entities
{
    public class Aggregation
    {
        public int Id { get; set; }

        public int StationId { get; set; }

        public int PollutantId { get; set; }

        public string OriginalMessage { get; set; }

        public string Value { get; set; }
            
    }
 
}
