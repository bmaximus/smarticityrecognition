﻿namespace SmartCityRecognitionApi.Entities
{
    public class Pollutant
    {
        public string Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Uom { get; set; }

        public string Order { get; set; }
    }

    
}