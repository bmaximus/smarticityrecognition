﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartCityRecognitionApi.Entities
{

    public class Province
    {
        public Province()
        {
            StationIds = new List<string>();
        }

        public string Id { get; set; }

        public string Name { get; set; }

        public int Order { get; set; }

        public List<string> StationIds { get; set; }
    }

    public class ProvinceMap
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string StationId { get; set; }
    }
}
