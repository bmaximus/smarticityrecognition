﻿namespace SmartCityRecognitionApi.Entities
{
    public class Observation
    {
        public string Id { get; set; }
        public string PollutantId { get; set; }
        public string StationId { get; set; }
        public string Value { get; set; }
        public string Datetime { get; set; }
    }

    
}